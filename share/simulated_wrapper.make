# Author: Frédéric Fort
.SUFFIXES:
.PHONY: all clean dirs

# PLU_SRC is the name of the prelude file with extension and of the future binary
#PLU_SRC=
# PLU_NODE is the name of the main node of the program
#PLU_NODE=

# USER_C_NODES is the name of the file(s) with extension containing the user code
#USER_C_NODES=

# In case of a local install, define this to the appropriate value and other variables will auto-fill
#PLU_DIR=
# Otherwise, you will have to set those values manually
#PLU_BIN_DIR is the path to the directory containing the preludec binary, either set it or PLU_DIR
#PLU_BIN_DIR=
#PLU_LIB_DIR is the path to the directory containing the "prelude standard library"
#PLU_LIB_DIR=
#PLU_SHARE_DIR is the path to the directory containing the wrapper files
#PLU_SHARE_DIR=

# In case you want to set some standard make variables
#CFLAGS +=
#LDFLAGS +=
#INCLUDES +=
#LIBS +=
#PLUFLAGS +=

# >>>>>>>>>>
# "Administrative statements"

OBJS = $(addprefix _build/obj/,$(addsuffix .o, $(PLU_NODE)) $(patsubst %.c,%.o, $(WRAPPER) $(USER_C_NODES)))

EXEC = $(patsubst %.plu,%,$(PLU_SRC))

WRAPPER=simulated_wrapper.c

#Check that PLU_SRC is set
ifndef PLU_SRC
$(error PLU_SRC has not been set)
endif

# Check that PLU_NODE is set
ifndef PLU_NODE
$(error PLU_NODE has not been set)
endif

ifdef PLU_DIR
# If the user supplied a PLU_DIR, we can just look there
PLU_BIN_DIR ?= $(PLU_DIR)/bin
else ifndef PLU_BIN_DIR
# User has supplied nothing, we can't progress
$(error PLU_BIN_DIR has not been set, either set it or PLU_DIR)
endif
PLUC ?= $(PLU_BIN_DIR)/preludec

ifdef PLU_DIR
# If the user supplied a PLU_DIR, we can just look there
PLU_LIB_DIR ?= $(PLU_DIR)/lib
else ifndef PLU_LIB_DIR
# User has supplied nothing, we can't progress
$(error PLU_LIB_DIR has not been set, either set it or PLU_DIR)
endif

ifdef PLU_DIR
# If the user supplied a PLU_DIR, we can just look there
PLU_SHARE_DIR ?= $(PLU_DIR)/share
else ifndef PLU_SHARE_DIR
# User has supplied nothing, we can't progress
$(error PLU_SHARE_DIR has not been set, either set it or PLU_DIR)
endif

CFLAGS += -g -Wall -Wextra -pedantic
LDFLAGS += -g
INCLUDES += -I$(PLU_LIB_DIR) -I. -I_build/plu
PLUFLAGS += -with_encoding

VPATH=$(PLU_LIB_DIR) $(PLU_SHARE_DIR)

define CCOMPILE =
	$(CC) $(CFLAGS) -c $< -o _build/obj/$(subst .c,.o,$(notdir $<)) $(LIBS) $(INCLUDES)
endef

# End of "Administrative statements"
# <<<<<<<<<<

all: $(EXEC)

%: _build/%
	cp $< $@

_build/$(EXEC): $(OBJS) | _build/
	$(CC) $(LDFLAGS) $(LIBS) -o $@ $^

_build/obj/%.o: %.c | _build/obj/
	$(CCOMPILE)

_build/obj/%.o: _build/plu/%.c | _build/obj/
	$(CCOMPILE)

_build/plu/$(PLU_NODE).c _build/plu/$(PLU_NODE).h: $(PLU_SRC) | _build/plu/
	$(PLUC) $(PLUFLAGS) -node $(PLU_NODE) -d _build/plu $(PLU_SRC)

%/:
	@mkdir -p $@

clean:
	rm -rf $(EXEC) _build

dirs:
	mkdir _build
	mkdir _build/obj
	mkdir _build/plu
