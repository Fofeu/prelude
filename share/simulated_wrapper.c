#define MAX_DATE 1000

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "encoded_task_params.h"

struct pending_task
{
  int job_idx;
  int deadl_abs;
  struct encoded_task_params* task;
};
struct priority_queue {
  char* data;
  size_t count;
  size_t capacity;
  size_t size;
  int (*cmp)(const void*, const void*);
};

struct priority_queue* priority_queue_alloc(size_t capacity, size_t size, int (*cmp)(const void*, const void*));
bool priority_queue_pop(struct priority_queue* priority_queue, void* dst);
void priority_queue_push(struct priority_queue* queue, void* src);

int dword_lookup(struct dword_t* dword, size_t idx);
int pending_task_priority_cmp(const void* lhs, const void* rhs);

int main()
{
  struct encoded_task_params* task_set;
  int task_number;

  get_task_set(&task_number, &task_set);

  struct priority_queue* run_queue = priority_queue_alloc(100, sizeof(struct pending_task), pending_task_priority_cmp);
  size_t job_idx[task_number];
  memset(job_idx, 0, task_number*sizeof(size_t));

  for (int date = 0; date < MAX_DATE; ++date)
  {
    //check if tasks need to be enqueued
    for (int i = 0; i < task_number; ++i)
    {
      int offset = task_set[i].e_t_initial_release;
      int period = task_set[i].e_t_period;
      if (date >= offset && (((date - offset) % period) == 0))
      {
        int deadline = dword_lookup(&task_set[i].e_t_dword, job_idx[i]);
        struct pending_task new_pending = { .job_idx = job_idx[i], .deadl_abs = date + deadline, .task = &task_set[i] };
        job_idx[i] += 1;
        priority_queue_push(run_queue, &new_pending);
      }
    }

    //While there is a task that requires to be run
    struct pending_task to_run;
    while (priority_queue_pop(run_queue, &to_run))
    {
      to_run.task->e_t_body(NULL);
    }
  }

  return 0;

}

struct priority_queue* priority_queue_alloc(size_t capacity, size_t size, int (*cmp)(const void*, const void*))
{
  struct priority_queue* queue = malloc(sizeof(struct priority_queue));
  queue->data = malloc(size*capacity);
  queue->count = 0;
  queue->capacity = capacity;
  queue->size = size;
  queue->cmp = cmp;

  return queue;
}

bool priority_queue_pop(struct priority_queue* queue, void* dst)
{
  if (queue->count > 0)
  {
    memcpy(dst, queue->data, queue->size);
    queue->count -= 1;
    if (queue->count > 0)
    {
      memmove(queue->data, queue->data+queue->size, queue->size*queue->count);
    }
    return true;
  }
  else
  {
    return false;
  }
}

void priority_queue_push(struct priority_queue* queue, void* src)
{
  if (queue->capacity == 0)
  {
    queue->data = malloc(queue->size);
    queue->capacity = 1;
  }
  while (queue->capacity <= queue->size)
  {
    queue->data = realloc(queue->data, queue->capacity*2);
  }

  memcpy(queue->data+(queue->size*queue->count), src, queue->size);
  queue->count += 1;
  qsort(queue->data, queue->count, queue->size, queue->cmp);

}

int dword_lookup(struct dword_t* dword, size_t idx)
{
  if (idx < (size_t)dword->pref_size)
  {
    return dword->pref[idx];
  }
  else
  {
    return dword->pat[(idx-dword->pref_size)%dword->pat_size];
  }
}

int pending_task_priority_cmp(const void* lhs, const void* rhs)
{
  const int l = ((const struct pending_task*)lhs)->deadl_abs;
  const int r = ((const struct pending_task*)rhs)->deadl_abs;
  if (l < r)
    return -1;
  if (l > r)
    return 1;
  return 0;
}
