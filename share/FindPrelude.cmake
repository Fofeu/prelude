# - Find Prelude compiler
# Find the Prelude synchronous language compiler with associated includes path.
# See http://www.lifl.fr/~forget/prelude.html
# and https://forge.onera.fr/projects/prelude
# This module defines
#  PRELUDE_COMPILER, the prelude compiler
#  PRELUDE_COMPILER_VERSION, the version of the prelude compiler
#  PRELUDE_GENWRAPPER, the prelude wrapper generator (since Prelude v1.6 and up)
#  PRELUDE_INCLUDE_DIR, where to find dword.h, etc.
#  PRELUDE_FOUND, If false, Prelude was not found.
# On can set PRELUDE_PATH_HINT before using find_package(Prelude) and the
# module with use the PATH as a hint to find preludec.
#
# The hint can be given on the command line too:
#   cmake -DPRELUDE_PATH_HINT=/DATA/ERIC/Prelude/prelude-x.y /path/to/source
#
# The module defines some functions:
#   Prelude_Compile(NODE <Prelude Main Node>
#                   PLU_FILES <Prelude files>
#                  [USER_C_FILES <C files>]
#                  [NOENCODING]
#                  [WITHENCODING]
#                  [REAL_IS_DOUBLE]
#                  [BOOL_IS_STDBOOL]
#                  [EXTERN_BUFFERS]
#                  [TRACING fmt])
#
# When used the Prelude_Compile macro define the variable
# <Prelude Main Node>_PLU_LIBNAME in the parent scope so that
# one can refer to the library target in parent scope.
#
#
#   Prelude_GenWrapper(NODE <Prelude Main Node>
#                      PLU_FILES <Prelude files>
#                      [TRACING fmt])
#
# When used the Prelude_GenWrapper macro define the variable
# <Prelude Main Node>_WRAPPER_SOURCES in the parent scope so that
# one can refer to generated wrapper files in the parent scope.
#

if(PRELUDE_PATH_HINT)
  message(STATUS "FindPrelude: using PATH HINT: ${PRELUDE_PATH_HINT}")
else()
  set(PRELUDE_PATH_HINT)
endif()

#One can add his/her own builtin PATH.
#FILE(TO_CMAKE_PATH "/DATA/ERIC/Prelude/prelude-x.y" MYPATH)
#list(APPEND PRELUDE_PATH_HINT ${MYPATH})

# FIND_PROGRAM twice using NO_DEFAULT_PATH on first shot
find_program(PRELUDE_COMPILER
  NAMES preludec
  PATHS ${PRELUDE_PATH_HINT}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH
  DOC "Path to the Prelude compiler command 'preludec'")

find_program(PRELUDE_COMPILER
  NAMES preludec
  PATHS ${PRELUDE_PATH_HINT}
  PATH_SUFFIXES bin
  DOC "Path to the Prelude compiler command 'preludec'")

if(PRELUDE_COMPILER)
    # get the path where the prelude compiler was found
    get_filename_component(PRELUDE_PATH ${PRELUDE_COMPILER} PATH)
    # remove bin
    get_filename_component(PRELUDE_PATH ${PRELUDE_PATH} PATH)
    # add path to PRELUDE_PATH_HINT
    list(INSERT PRELUDE_PATH_HINT 0 ${PRELUDE_PATH})
    execute_process(COMMAND ${PRELUDE_COMPILER} -version
        OUTPUT_VARIABLE PRELUDE_COMPILER_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    message(STATUS "Prelude compiler version is : ${PRELUDE_COMPILER_VERSION}")
    execute_process(COMMAND ${PRELUDE_COMPILER} -help
        OUTPUT_VARIABLE PRELUDE_OPTIONS_LIST
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    set(PRELUDE_TRACING_OPTION)
    string(REGEX MATCH "-tracing output" PRELUDE_TRACING_OPTION "${PRELUDE_OPTIONS_LIST}")
    if (PRELUDE_TRACING_OPTION)
      message(STATUS "Prelude compiler support -tracing.")
      set(PRELUDE_SUPPORT_TRACING "YES")
    else(PRELUDE_TRACING_OPTION)
      message(STATUS "Prelude compiler DOES NOT support -tracing.")
      set(PRELUDE_SUPPORT_TRACING "NO")
    endif(PRELUDE_TRACING_OPTION)

    # prelude compiler 1.6 and up should come with PRELUDE_GENWRAPPER as well
    if (PRELUDE_COMPILER_VERSION VERSION_GREATER "1.5")
      find_program(PRELUDE_GENWRAPPER
        NAMES prelude_genwrapper
        PATHS ${PRELUDE_PATH_HINT}
        PATH_SUFFIXES bin
        DOC "Path to the Prelude wrapper generator tool command 'prelude_genwrapper'")
      if (PRELUDE_GENWRAPPER)
        message(STATUS "Prelude wrapper generator is a separate command: ${PRELUDE_GENWRAPPER}")
      else()
        message(WARNING "Prelude wrapper generator wasn't found despite 1.6+ version: ${PRELUDE_COMPILER_VERSION}")
      endif()
    endif()
endif(PRELUDE_COMPILER)

find_path(PRELUDE_INCLUDE_DIR
          NAMES dword.h
          PATHS ${PRELUDE_PATH_HINT}
          PATH_SUFFIXES lib/prelude
          DOC "The Prelude include headers")

# Check if LTTng is to be supported
if (NOT LTTNG_FOUND)
  option(ENABLE_LTTNG_SUPPORT "Enable LTTng support" OFF)
  if(ENABLE_LTTNG_SUPPORT)
    find_package(LTTng)
    if (LTTNG_FOUND)
      message(STATUS "Will build LTTng support into library...")
      include_directories(${LTTNG_INCLUDE_DIR})
    endif(LTTNG_FOUND)
  endif(ENABLE_LTTNG_SUPPORT)
endif()

# Macros used to compile a prelude library
include(CMakeParseArguments)
function(Prelude_Compile)
  set(options WITHENCODING NOENCODING REAL_IS_FLOAT REAL_IS_DOUBLE BOOL_IS_INT BOOL_IS_STDBOOL EXTERN_BUFFERS)
  set(oneValueArgs NODE TRACING)
  set(multiValueArgs PLU_FILES USER_C_FILES)
  cmake_parse_arguments(PLU "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if (PRELUDE_COMPILER_VERSION VERSION_GREATER "1.5")
    # The default behaviour of prelude is now NOENCODING
    set(PRELUDE_ENCODING "")
    set(PRELUDE_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PLU_NODE}/noencoding")
    set(PRELUDE_ENCODING_SUFFIX "-noencoding")
    # But we may still require encoding
    if(PLU_WITHENCODING)
      set(PRELUDE_ENCODING "-with_encoding")
      set(PRELUDE_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PLU_NODE}/encoded")
      set(PRELUDE_ENCODING_SUFFIX "-encoded")
    endif()
    if(PLU_EXTERN_BUFFERS)
      set(PRELUDE_EXTERN_BUFFERS_OPT "-extern_buffers")
    endif()
  else ()
    if(PLU_NOENCODING)
      set(PRELUDE_ENCODING "-no_encoding")
      set(PRELUDE_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PLU_NODE}/noencoding")
      set(PRELUDE_ENCODING_SUFFIX "-noencoding")
    else()
      set(PRELUDE_ENCODING)
      set(PRELUDE_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PLU_NODE}/encoded")
      set(PRELUDE_ENCODING_SUFFIX "-encoded")
    endif()
  endif()

  set(PLUOPT "REAL_IS_DOUBLE;REAL_IS_FLOAT;BOOL_IS_STDBOOL;BOOL_IS_INT")
  foreach (OPT IN LISTS PLUOPT)
    string(TOLOWER ${OPT} LOPT)
    if (PLU_${OPT})
      set(PRELUDE_${OPT}_OPT "-${LOPT}")
    else()
      set(PRELUDE_${OPT}_OPT "")
    endif()
  endforeach()

  if (PRELUDE_SUPPORT_TRACING)
    if(PLU_TRACING)
      set(PRELUDE_TRACING_OPT "-tracing")
      set(PRELUDE_TRACING_OPT_VALUE "${PLU_TRACING}")
    else()
      set(PRELUDE_TRACING_OPT "-tracing")
      set(PRELUDE_TRACING_OPT_VALUE "no")
    endif()
  else(PRELUDE_SUPPORT_TRACING)
    set(PRELUDE_TRACING_OPT "")
    set(PRELUDE_TRACING_OPT_VALUE "")
  endif(PRELUDE_SUPPORT_TRACING)

  file(MAKE_DIRECTORY ${PRELUDE_OUTPUT_DIR})
  set(PRELUDE_GENERATED_FILES
      ${PRELUDE_OUTPUT_DIR}/${PLU_NODE}.c
      ${PRELUDE_OUTPUT_DIR}/${PLU_NODE}.h)

  add_custom_command(
      OUTPUT ${PRELUDE_GENERATED_FILES}
      COMMAND ${PRELUDE_COMPILER} ${PRELUDE_ENCODING} ${PRELUDE_BOOL_IS_STDBOOL_OPT} ${PRELUDE_REAL_IS_DOUBLE_OPT} ${PRELUDE_TRACING_OPT} ${PRELUDE_TRACING_OPT_VALUE} ${PRELUDE_EXTERN_BUFFERS_OPT} -d ${PRELUDE_OUTPUT_DIR} -node ${PLU_NODE} ${PLU_PLU_FILES}
      DEPENDS ${PLU_PLU_FILES}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      COMMENT "Compile prelude source(s): ${PLU_PLU_FILES})"
  )
  set_source_files_properties(${PRELUDE_GENERATED_FILES}
                              PROPERTIES GENERATED TRUE)
  include_directories(${PRELUDE_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${PRELUDE_OUTPUT_DIR})
  add_library(${PLU_NODE}${PRELUDE_ENCODING_SUFFIX} SHARED
              ${PRELUDE_GENERATED_FILES} ${PLU_USER_C_FILES}
              )
  # export Generated prelude library name to parent scope
  set(${PLU_NODE}_PLU_LIBNAME ${PLU_NODE}${PRELUDE_ENCODING_SUFFIX} PARENT_SCOPE)
  if(LTTNG_FOUND)
    target_link_libraries(${PLU_NODE}${PRELUDE_ENCODING_SUFFIX} ${LTTNG_LIBRARIES})
  endif()
  message(STATUS "Prelude: Added rule for building prelude library: ${PLU_NODE}${PRELUDE_ENCODING_SUFFIX} (compile opts= ${PRELUDE_ENCODING} ${PRELUDE_BOOL_IS_STDBOOL_OPT} ${PRELUDE_REAL_IS_DOUBLE_OPT} ${PRELUDE_TRACING_OPT} ${PRELUDE_TRACING_OPT_VALUE} ${PRELUDE_EXTERN_BUFFERS_OPT})")
endfunction(Prelude_Compile)

function(Prelude_GenWrapper)
  set(options "")
  set(oneValueArgs NODE WRAPPER_TYPE WRAPPER_DIR LUSI_FILENAME)
  set(multiValueArgs PLU_FILES)
  cmake_parse_arguments(PLUW "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if (PRELUDE_COMPILER_VERSION VERSION_GREATER "1.5")
     # create wrapper directory (if it does not exist)
     file(MAKE_DIRECTORY ${PLUW_WRAPPER_DIR})
     set(${PLUW_NODE}_WRAPPER_SOURCES
         ${PLUW_WRAPPER_DIR}/${PLUW_NODE}_includes_sensors_actuators.c
         ${PLUW_WRAPPER_DIR}/${PLUW_NODE}_includes_sensors_actuators.h
         ${PLUW_WRAPPER_DIR}/${PLUW_NODE}_includes_lustre_wrapper.c
         ${PLUW_WRAPPER_DIR}/${PLUW_NODE}_includes.h
         )
     set(${PLUW_NODE}_WRAPPER_SOURCES ${${PLUW_NODE}_WRAPPER_SOURCES} PARENT_SCOPE)
     
     add_custom_command(
       OUTPUT  ${${PLUW_NODE}_WRAPPER_SOURCES}
       COMMAND ${PRELUDE_GENWRAPPER} -gentype ${PLUW_WRAPPER_TYPE} -d ${PLUW_WRAPPER_DIR} -lusi ${PLUW_LUSI_FILENAME} -node  ${PLUW_NODE} ${PLUW_PLU_FILES}
       #DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${PLUW_PLU_FILES} ${CMAKE_CURRENT_SOURCE_DIR}/${PLUW_LUSI_FILENAME}
       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
       COMMENT "(Re)Generate wrappers from lustre/prelude source(s): ${PLUW_PLU_FILES} ${PLUW_LUSI_FILENAME}"       
     )

     add_custom_target(
       ${PLUW_NODE}_GenWrappers
       COMMAND ${CMAKE_COMMAND} -E echo "Forcibly generate wrappers"
       DEPENDS ${${PLUW_NODE}_WRAPPER_SOURCES}
       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
       COMMENT "Explicitely regenerate wrappers"
     )

     set_source_files_properties(${${PLUW_NODE}_WRAPPER_SOURCES} PROPERTIES GENERATED TRUE)
     include_directories(${PLUW_WRAPPER_DIR})

     message(STATUS "Prelude: Added rule for generating wrappers (type=${PLUW_WRAPPER_TYPE}): node ${PLUW_NODE} in ${PLUW_WRAPPER_DIR}")

  else ()
    message (ERROR "Prelude ${PRELUDE_COMPILER_VERSION} does not support Prelude_GenWrapper")
  endif()

endfunction(Prelude_GenWrapper)

# handle the QUIETLY and REQUIRED arguments and set PRELUDE_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PRELUDE
                                  REQUIRED_VARS PRELUDE_COMPILER PRELUDE_GENWRAPPER PRELUDE_INCLUDE_DIR)
# VERSION FPHSA options not handled by CMake version < 2.8.2)
#                                  VERSION_VAR PRELUDE_COMPILER_VERSION)
mark_as_advanced(PRELUDE_INCLUDE_DIR)
