/* ----------------------------------------------------------------------------
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- */


// Authors: Titouan ROOS, Julien FORGET, Frédéric FORT

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ptask.h"
#include "pbarrier.h"
#include "nonencoded_aer_task_params.h"
#include "multirate_precedence.h"

//Set this to a higher value, if things are too fast
#define SLOWDOWN 1

struct task_args
{
  struct nonencoded_aer_task_params* params;
  struct task_precedences* precs;
  sem_t **src_sems;
  sem_t **dst_sems;
};

int must_wait(const int n, const int job_id, const struct job_prec* pref, const int pref_size, const struct job_prec* pat, const int pat_size);

int must_post(const int n, const int job_id, const struct job_prec* pref, const int pref_size, const struct job_prec* pat, const int pat_size);

void task_body()
{
  struct task_args *args = ptask_get_argument();
  int job_id = 0;

  while(1)
  {
    for(int i = 0; i < args->precs->prec_src_size; ++i)
    {
      if(must_wait(job_id, args->precs->prec_src[i].dst_period,
                   args->precs->prec_src[i].prec_pref, args->precs->prec_src[i].prec_pref_size,
                   args->precs->prec_src[i].prec_pat, args->precs->prec_src[i].prec_pat_size))
      {
        sem_wait(args->src_sems[i]);
      }
    }

    // call step function
    if (args->params->nea_t_A)
      args->params->nea_t_A(NULL);
    if (args->params->nea_t_E)
      args->params->nea_t_E(NULL);
    if (args->params->nea_t_R)
      args->params->nea_t_R(NULL);

    for(int i = 0; i < args->precs->prec_dst_size; ++i)
    {
      if (must_post(job_id, args->precs->prec_dst[i].src_period,
                    args->precs->prec_dst[i].prec_pref, args->precs->prec_dst[i].prec_pref_size,
                    args->precs->prec_dst[i].prec_pat, args->precs->prec_dst[i].prec_pat_size))
      {
        sem_post(args->dst_sems[i]);
      }
    }

    job_id += 1;

    ptask_wait_for_period();
  }
}

int main()
{
  struct nonencoded_aer_task_params *task_set;
  struct task_precedences *prec_set;
  tpars param;
  int task_number, prec_number;

  ptask_init(SCHED_DEADLINE, PARTITIONED, PRIO_INHERITANCE);

  // get Prelude task set
  get_task_set(&task_number, &task_set);
  get_sorted_precedence_set(&prec_number, &prec_set);

  int task_pid[task_number];
  struct task_args args[task_number];
  sem_t sems[prec_number];
  sem_t* task_sems[prec_number][2][task_number];
  int sem_size = 0;

  for(int i=0; i<task_number; ++i)
    for(int j=0; j<2; ++j)
      for(int k=0; k<prec_number; ++k)
        task_sems[i][j][k] = NULL;

  //populate task arguments
  for (int i=0; i<task_number; i++)
  {
    args[i].params = &task_set[i];
    args[i].src_sems = task_sems[i][0];
    args[i].dst_sems = task_sems[i][1];
    args[i].precs = &prec_set[i];
  }

  for(int i=0; i<prec_number; i++)
  {
    sem_init(&sems[i], 0, 0);
  }

  for (int i=0; i<task_number; i++)
    for (int j=0; j<prec_set[i].prec_dst_size; j++)
    {
      args[i].dst_sems[j] = &sems[sem_size];
      for (int k=0; k<task_number; k++)
        if (!strcmp(prec_set[i].prec_dst[j].dst_name, prec_set[k].task_name))
          for (int l=0; l<prec_set[k].prec_src_size; l++)
            if (!strcmp(prec_set[k].prec_src[l].src_name, prec_set[i].task_name))
              args[k].src_sems[l] = &sems[sem_size];
      sem_size += 1;
    }

  // create tasks
  for (int i=0; i<task_number; i++)
  {
    ptask_param_init(param);
    ptask_param_argument(param, &args[i]);
    ptask_param_period(param, SLOWDOWN*(ptime)(task_set[i].nea_t_period), MILLI);
    ptask_param_deadline(param, SLOWDOWN*(ptime)task_set[i].nea_t_deadline, MILLI);
    ptask_param_runtime(param, SLOWDOWN*(ptime)(task_set[i].nea_t_wcet), MILLI);
    ptask_param_processor(param, i%2); // multi-core scheduling
    ptask_param_activation(param, DEFERRED);

    task_pid[i] = ptask_create_param(task_body, &param);

    if (task_pid[i]<0) {
      fprintf(stderr,"Could not create task %s\n",task_set[i].nea_t_name);
      exit(-1);
    }
    //printf("Created task %s with dd %d\n", task_set[i].nea_t_name, (int)deadline);
  }
  printf("All tasks created\n");

  for (int i= 0; i<task_number; i++)
  {
    if (task_set[i].nea_t_I != NULL)
    {
      ptask_param_init(param);
      ptask_param_processor(param, i%2);

      int pid = ptask_create_param((void(*)(void))task_set[i].nea_t_I, &param);

      if (pid<0)
      {
        fprintf(stderr, "Could not create task %s_I\n", task_set[i].nea_t_name);
        exit(-1);
      }

      pthread_join(ptask_get_threadid(pid), 0);
    }
  }

  ptime now = ptask_gettime(MILLI);
  for (int i=0; i<task_number; i++)
  {
    ptime offt = 10 + now;
    int r = ptask_activate_at(i, offt, MILLI);
    if (r < 0)
    {
      printf("Could not activate task %s\n", task_set[i].nea_t_name);
    }
  }
  printf("All tasks activated\n");


  // Wait for tasks to finish even if they shouldn't
  for(int i=0; i<task_number; i++)
  {
    pthread_join(ptask_get_threadid(task_pid[i]), 0);
  }

  // This should never return
  return 1;
}

inline int must_wait
(const int job_id, const int period,
 const struct job_prec* pref, const int pref_size,
 const struct job_prec* pat, const int pat_size)
{
  if (job_id < pref_size)
  {
    return job_id % period == pref[job_id].dst_job;
  }
  else
  {
    return job_id % period == pat[(job_id-pref_size)%pat_size].dst_job;
  }
}

inline int must_post
(const int job_id, const int period,
 const struct job_prec* pref, const int pref_size,
 const struct job_prec* pat, const int pat_size)
{
  if (job_id < pref_size)
  {
    return job_id % period == pref[job_id].src_job;
  }
  else
  {
    return job_id % period == pat[(job_id-pref_size)%pat_size].src_job;
  }
}
