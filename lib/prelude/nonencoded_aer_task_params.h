/* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- */

#ifndef _nonencoded_aer_task_params_H
#define _nonencoded_aer_task_params_H
// Description of a real time task, without precedence encoding, with the AER format.

struct nonencoded_aer_task_params {
  char* nea_t_name;
  int nea_t_period;
  int nea_t_initial_release;
  int nea_t_wcet;
  int nea_t_deadline;
  int (*nea_t_A)(void*); // Input acquisition step
  int (*nea_t_E)(void*); // Execution step
  int (*nea_t_R)(void*); // Output restitution step
  int (*nea_t_I)(void*); // Initialization step
};

void get_task_set(int* task_number, struct nonencoded_aer_task_params** task_set);

#endif
