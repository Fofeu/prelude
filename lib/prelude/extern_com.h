/* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- */

#ifndef extern_com_H_
#define extern_com_H_

struct buffer_desc {
  const int elem_size;
  const int buffer_size;
  void (* const init)(int, int);
  void *addr; // that's odd, I don't see how this info could be generated...
};

extern void get_buffer_set (int* buffer_number, struct buffer_desc** buffer_set);

void alloc_buffers();
void read_val(int id, int idx, int data_size, void *data);
void write_val(int id, int idx, int data_size, const void *data);

#endif
