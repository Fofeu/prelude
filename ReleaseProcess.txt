This file tries to describe the current Prelude release process.
Current version is written for a Prelude administrator
and may certainly be only readable for them.

Prelude releases should be done with the following steps:

1) Review currently opened and fixed bug/task in the tracker

    Issue (either bugs or evolution):
    https://forge.onera.fr/projects/prelude/issues
    
2) Edit prelude/configure.ac
   and bump the version to the right number with
   [example for prelude-1.6.0]

   AC_INIT([Prelude], [1.6.0], [])


3) commit the modified files
   prelude/configure.ac
   with a meaningful comment saying you are preparing a Prelude version

4) Do a fresh checkout somewhere
    Build prelude from your pristine source tree
    svn co https://svn.onera.fr/Prelude/Prelude/trunk prelude-1.6.0

5) (optional) do any testing you usually do, at least check compilation:

    cd prelude-1.6.0
    autoconf
    ./configure
    make


6) Tag prelude tree with appropriate version

     svn copy https://svn.onera.fr/Prelude/Prelude/trunk https://svn.onera.fr/Prelude/Prelude/tags/prelude-1.6.0

7) Make prelude packages

    svn export . $HOME/tmp/prelude-1.6.0
    cd $HOME/tmp/prelude-1.6.0
    cd ..
    tar zcvf prelude-1.6.0-src.tgz prelude-1.6.0

8) (optional) Tests your packages or ask for help to test them
    
9) put your package in
    https://forge.onera.fr/projects/prelude/files

10) Advertise your freshly release package on every social network you belong to.

You are right this process IS painful and should be automated (a lot...)
be sure we are working on it :))

12) Re-edit prelude/configure.ac 
  and bump the version to the the next number with svn
   [example for prelude-1.7-svn-svnversion]

   AC_INIT([Prelude], [1.7.svn-svnversion], [])
