# ----------------------------------------------------------------------------
# SchedMCore - A MultiCore Scheduling Framework
# Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
#
# This file is part of Prelude
#
# Prelude is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation ; either version 2 of
# the License, or (at your option) any later version.
#
# Prelude is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY ; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program ; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA
#----------------------------------------------------------------------------

#LOCAL_DOCDIR=ocamldoc

#DIST_FILES_DIR= \
#  AUTHORS.txt \
#  configure \
#  configure.ac \
#  emacs \
#  examples \
#  INSTALL \
#  install.sh \
#  lib \
#  LICENSE-LGPL.txt \
#  Makefile.in \
#  myocamlbuild.ml \
#  README \
#  share \
#  src \
#  WCET_aware

all: preludec prelude_genwrapper

preludec:
	dune build src/main.exe
	cp _build/default/src/main.exe preludec

prelude_genwrapper:
	dune build src/genwrappermain.exe
	cp _build/default/src/genwrappermain.exe prelude_genwrapper

doc:
	dune build @doc

install: all
	dune build @install
	dune install

clean:
	dune clean

# Broken until we use git
#dist: distclean
#	rm -rf @PACKAGE_TARNAME@-@PACKAGE_VERSION@
#	mkdir @PACKAGE_TARNAME@-@PACKAGE_VERSION@
#	cp -r $(DIST_FILES_DIR) @PACKAGE_TARNAME@-@PACKAGE_VERSION@
#	tar cvzf @PACKAGE_TARNAME@-@PACKAGE_VERSION@.tgz @PACKAGE_TARNAME@-@PACKAGE_VERSION@
#	rm -rf @PACKAGE_TARNAME@-@PACKAGE_VERSION@

.PHONY: preludec prelude_genwrapper doc install uninstall clean distclean dist
