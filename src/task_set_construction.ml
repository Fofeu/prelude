(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Buil a task set from a task graph *)

open Corelang
open Task_graph
open Task_set

let add_constant task_set (cst,tag) =
  let t =
    {task_id = (cst,tag);
     task_kind = CstTask;
     task_inputs = [];
     task_outputs = [];
     task_conds = [];
     task_wcet = 0;
     task_deadline = Deadlines.dword_create 0;
     task_period = 0;
     task_release = 0}
  in
  Hashtbl.add task_set t.task_id t

let add_var t ndesc is_output is_input =
  match ndesc.ndvar_kind with (* TODO: check conditions order *)
  | WhenVar ->
      begin
        let cond = {cond_var = ndesc.ndvar_var_decl;
                    cond_pred = dummy_vref,dummy_read_pattern;
                    cond_val=true} in
        t.task_conds <- cond::t.task_conds
      end
  | WhennotVar ->
      begin
        let cond = {cond_var = ndesc.ndvar_var_decl;
                    cond_pred = dummy_vref,dummy_read_pattern;
                    cond_val=false} in
        t.task_conds <- cond::t.task_conds
      end
  | RegularVar ->
      let v = ndesc.ndvar_var_decl in
      if is_output then
        t.task_outputs <- (List.merge
                             (fun (v1,_) (v2,_) -> compare v1.var_order v2.var_order)
                             [(v,[])] t.task_outputs);
      if is_input then
        t.task_inputs <- (List.merge
                            (fun (v1,_,_) (v2,_,_) -> compare v1.var_order v2.var_order)
                            [(v,dummy_vref,dummy_read_pattern)] t.task_inputs)

(** Adds a task for the imported node vertex [v] belongs to *)
let add_vertex task_set _ v =
  match v.vertex_desc with
  | Var _ ->
      () (* Main var, added previously *)
  | Constant (cst,tag) ->
      add_constant task_set (cst,tag)
  | NodeVar ndesc ->
      let tid = taskid_of_vertex v in
      let t = try Hashtbl.find task_set tid with
      | Not_found ->
          begin
            let ck_nd = ndesc.ndvar_clock in
            let nd = ndesc.ndvar_node_desc in
            let task_kind = task_kind ndesc in
            let ck = Clocks.pclock_parent (Clocks.clock_of_impnode_clock ck_nd) in
            let period = Clocks.period ck in
            let (release,_) = Clocks.phase ck in
            let dd = Deadlines.dword_create period in
            let t =
              {task_id = (ndesc.ndvar_node_desc.nodei_id,ndesc.ndvar_tag);
               task_kind = task_kind;
               task_inputs = [];
               task_outputs = [];
               task_conds = [];
               task_wcet = nd.nodei_wcet;
               task_deadline = dd;
               task_period = period;
               task_release = release}
            in
            Hashtbl.add task_set t.task_id t;
            t
          end
      in
      add_var t ndesc (is_output v) (is_input v)
  | PredefOpVar _ -> failwith "Internal error"

(** Adds the sensor corresponding to main node input [v]  *)
let add_sensor task_set env v =
  let sdecl = try
    Iterenv.get env v.var_id
  with Not_found ->
    raise (Error (v.var_loc, No_sensor_wcet v.var_id))
  in
  let wcet = match sdecl.top_decl_desc with
  | SensorDecl s -> s.sensor_wcet
  | _ -> raise (Error (v.var_loc, Not_sensor v.var_id))
  in
  let ck = Clocks.pclock_parent v.var_clock in
  let period = Clocks.period ck in
  let (release,_) = Clocks.phase ck in
  let dd = Deadlines.dword_create period in
  let outputs = [v,[]] in
  let t =
    {task_id = (v.var_id,0);
     task_kind = Sensor;
     task_inputs = [];
     task_outputs = outputs;
     task_conds = [];
     task_wcet = wcet;
     task_deadline = dd;
     task_period = period;
     task_release = release}
  in
  Hashtbl.add task_set t.task_id t

(** Adds the actuator corresponding to main node output [v]  *)
let add_actuator task_set env v =
  let adecl = try
    Iterenv.get env v.var_id
  with Not_found ->
    raise (Error (v.var_loc, No_actuator_wcet v.var_id))
  in
  let wcet = match adecl.top_decl_desc with
  | ActuatorDecl a -> a.actuator_wcet
  | _ -> raise (Error (v.var_loc, Not_actuator v.var_id))
  in
  let ck = Clocks.pclock_parent v.var_clock in
  let period = Clocks.period ck in
  let (release,_) = Clocks.phase ck in
  let dd =
    match v.var_dec_deadline with
    | None -> Deadlines.dword_create period
    | Some d -> Deadlines.dword_create (min period d)
  in
  let inputs = [v,dummy_vref,dummy_read_pattern] in
  let t =
    {task_id = (v.var_id,0);
     task_kind = Actuator;
     task_inputs = inputs;
     task_outputs = [];
     task_conds = [];
     task_wcet = wcet;
     task_deadline = dd;
     task_period = period;
     task_release = release}
  in
  Hashtbl.add task_set t.task_id t

(** Build a task set from a task graph *)
let of_task_graph g exp_main prog =
  let env = Iterenv.skip_until prog exp_main.node_id in
  let task_set = Hashtbl.create 30 in
  List.iter (add_sensor task_set env) exp_main.node_inputs;
  List.iter (add_actuator task_set env) exp_main.node_outputs;
  (* Create tasks *)
  Hashtbl.iter (fun _ v -> add_vertex task_set g v) g;
  (* Deadline calculus*)
  if (not !Options.no_encoding) then
    Deadline_calculus.dd_prog g task_set exp_main;
  (* Communication protocol *)
  Com_protocol.proto_prog g task_set;
  task_set
