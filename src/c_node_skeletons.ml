(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generate node/sensor/actuator C skeletons. For genwrapper. *)

open C_utils
open Corelang
open Task_set
open Format

(** print type name of first var in decls *)
let pp_var_decls_rettype out_f decls =
  let first = List.hd decls in
  fprintf out_f "%s " (tystring_of_type first.var_type);;

(** print list of var decls used as formal arguments
    with type and name: typ1 name1, typ2 name2, ... *)
let pp_var_decls_formal_args out_f decls =
  let first = List.hd decls in
  fprintf out_f "%s" (string_of_type_var first.var_type first.var_id);
  if (List.length decls > 1) then
    List.iter
      (fun (vd) -> fprintf out_f ", %s" (string_of_type_var vd.var_type vd.var_id))
      (List.tl decls)

(** print list of var decls used as actual arguments name:
    name1, name2, ... *)
let pp_var_decls_actual_args out_f decls vfmtfirst vfmtother =
  let first = List.hd decls in
  fprintf out_f vfmtfirst first.var_id;
  if (List.length decls > 1) then
    List.iter
      (fun (vd) -> fprintf out_f vfmtother vd.var_id)
      (List.tl decls)

(** print list of var decls for static var definition *)
let pp_var_decls_static_define out_f inputs decls =
  List.iter
    (fun (vd) ->
      (* Do not redeclare a variable when it is both an input and an output *)
      if (not (List.exists (fun i -> i.var_id = vd.var_id) inputs)) then
          fprintf out_f "static %s;@," (string_of_type_var vd.var_type vd.var_id))
    decls

(** print one imported header (or body) *)
let pp_imported_node out_f body forlustrec desc =
  match desc.top_decl_desc with
  | ImportedNode n ->
    (* print return type *)
    if (List.length n.nodei_outputs = 1) then
      begin
        fprintf out_f "@[< 2>";
        (pp_var_decls_rettype out_f n.nodei_outputs)
      end
    else
      begin
        if not body then
          begin
            fprintf out_f "@[<v> /* forward declaration of structure type */@,";
            fprintf out_f "%s ;@,@]" (outstruct_type (n.nodei_id,0))
          end;
        fprintf out_f "@[< 2>void "
      end;

    (* now print function name and input param *)
    fprintf out_f "%s(" n.nodei_id;
    pp_var_decls_formal_args out_f n.nodei_inputs;
    if (List.length n.nodei_outputs > 1) then
      fprintf out_f ", %s* outputs" (outstruct_type (n.nodei_id,0));
    fprintf out_f ")@]";
    if (not body) then
      fprintf out_f ";@,@,"
    else
      if forlustrec then
        begin
          fprintf out_f "@,@[<v 2>{@,";
          (* declare static var for storing outputs *)
          fprintf out_f "/* declare outputs static var */@,";
          if (List.length n.nodei_outputs = 1) then
            begin
              pp_var_decls_static_define out_f n.nodei_inputs n.nodei_outputs;
            end
          else
            fprintf out_f "/* no need: 'outputs' passed as argument */@,";

          fprintf out_f "static int firstCall = 1;@,";
          fprintf out_f "/* static allocation of lustrec node state variable */@,";
          fprintf out_f "%s_ALLOC(static,self);@," n.nodei_id;
          fprintf out_f "/* guard for resetting node state on first call */@,";
          fprintf out_f "@[<v 2>if (firstCall) {@,";
          fprintf out_f "%s_reset(&self);@," n.nodei_id;
          fprintf out_f "firstCall=0;";
          fprintf out_f "@]@,}@,";
          fprintf out_f "/* Call lustrec step function */@,";
          fprintf out_f "%s_step(" n.nodei_id;
          pp_var_decls_actual_args out_f n.nodei_inputs "%s" ", %s";
          fprintf out_f ", ";
          if (List.length n.nodei_outputs > 1) then
            pp_var_decls_actual_args out_f n.nodei_outputs "&(outputs->%s)" ", &(outputs->%s)"
          else
            pp_var_decls_actual_args out_f n.nodei_outputs "&%s" ", &%s";
          fprintf  out_f ", &self);@,";
          if (List.length n.nodei_outputs = 1) then
            begin
              fprintf out_f "return %s;@," (List.hd n.nodei_outputs).var_id;
            end
          else
            fprintf out_f "return;@,";
          fprintf out_f "@]@.} /* end of %s */ @]@\n@\n" n.nodei_id;
        end
      else
        begin
          fprintf out_f "@,@[<v 2>{@,";
          fprintf out_f "/* preludec: write/customize the imported node code right HERE */@,";
          fprintf out_f "/* declare outputs (static) var */@,";
          if (List.length n.nodei_outputs = 1) then
            begin
              pp_var_decls_static_define out_f n.nodei_inputs n.nodei_outputs;
              fprintf out_f "return %s;@," (List.hd n.nodei_outputs).var_id;
            end
          else
            begin
              fprintf out_f "/* no need: 'outputs' passed as argument */@,";
            end;
          fprintf out_f "@]}@,@.";
        end
  |_  -> ()

(** iterate over the task set and print *)
let pp_imported_nodes out_f prog body forlustrec =
  List.iter
  (pp_imported_node out_f body forlustrec)
  prog
    
(** print the imported node header file *)
let pp_imported_node_header prog basename =
  let in_header_basename = includes_filename !Options.main_node in
  let in_header_file  = (create_and_get_gendir basename) ^"/"^ in_header_basename ^".h" in
  let in_header_ch = open_out in_header_file in
  let in_header_f = formatter_of_out_channel in_header_ch in
  fprintf in_header_f "@[<v 0>/* File generated by preludec_genwrapper %s */@," Gwoptions.version;
  fprintf in_header_f "#ifndef _%s_H@," in_header_basename;
  fprintf in_header_f "#define _%s_H@,@," in_header_basename;
  pp_bool_include in_header_f;
  fprintf in_header_f "/* Actuators and Sensor prototype are declared in a separate file */@,";
  if !Options.io_code then
      fprintf in_header_f "#include  \"%s\"@,@," ((include_sens_act_filename !Options.main_node)^".h");
  pp_imported_nodes in_header_f prog false (Gwoptions.gen_IN_forLustrec());
  fprintf in_header_f "@]@,#endif@,@.";
  close_out in_header_ch

(** print the imported node body file *)
let pp_imported_node_body prog basename =
  let in_body_basename = includes_filename !Options.main_node in
  let in_body_file  = (create_and_get_gendir basename) ^"/"^ in_body_basename ^".c" in
  let in_body_ch = open_out in_body_file in
  let in_body_f = formatter_of_out_channel in_body_ch in
  fprintf in_body_f "@[<v 0>/* File generated by preludec_genwrapper %s */@," Gwoptions.version;
  fprintf in_body_f "#include  \"%s\"@,@]@," (in_body_basename ^".h");
  pp_imported_nodes in_body_f prog true false;
  fprintf in_body_f "@.";
  close_out in_body_ch

(** print the imported node body file *)
let pp_imported_node_lustrec_body prog basename =
  let in_body_basename = includes_filename !Options.main_node in
  let in_body_file  = (create_and_get_gendir basename) ^"/"^ in_body_basename ^"_lustre_wrapper.c" in
  let in_body_ch = open_out in_body_file in
  let in_body_f = formatter_of_out_channel in_body_ch in
  fprintf in_body_f "@[<v 0>/* File generated by preludec_genwrapper %s */@," Gwoptions.version;
  (* FIXME we may need to check whether if at least one lustre node is statefull other *_alloc.h file may not exist *)
  fprintf in_body_f "#include  \"%s\"@," (Filename.chop_extension !Gwoptions.lusi_filename ^"_alloc.h");
  fprintf in_body_f "#include  \"%s\"@," (in_body_basename ^".h");
  fprintf in_body_f "#include  \"%s\"@,@]@," (types_filename !Options.main_node ^".h");
  pp_imported_nodes in_body_f prog true true;
  fprintf in_body_f "@.";
  close_out in_body_ch

let pp_sensact_task out_f task body =
  match task.task_kind with
  | Sensor
      ->
    let firstTO = (List.hd task.task_outputs) in
    let (declTO,_) = firstTO in
    fprintf out_f "@[<v 2>%s " (tystring_of_type declTO.var_type);
    fprintf out_f "%s()" (sensor_name task.task_id);
    if body then
      begin
        fprintf out_f "@,{@,%s %s;@," (tystring_of_type declTO.var_type) declTO.var_id;
        fprintf out_f "/* put your sensor code here */@,";
        fprintf out_f "return %s;@,"  declTO.var_id;
        fprintf out_f "@]}@,@,";
      end
    else
      fprintf out_f "@];@,@,";
  | Actuator
      ->
    let firstTI = (List.hd task.task_inputs) in
    let (declTI,_,_) = firstTI in
    fprintf out_f "@[<v 2>void %s(%s %s)" (actuator_name task.task_id) (tystring_of_type declTI.var_type) declTI.var_id;
    if body then
      begin
        fprintf out_f "@,{@,/* put your actuator code here */@,";
        fprintf out_f "@]}@,@,";
      end
    else
      fprintf out_f "@];@,@,";
  | _ -> ()

let pp_sensact_tasks out_f task_set body =
  Hashtbl.iter (fun _ task -> pp_sensact_task out_f task body) task_set;
  fprintf out_f "@ "

(** print the sensor and actuator node header *)
let pp_sensor_actuator_header task_set basename =
  let task_set_cut =
    match task_set_bound with
    | Some n -> Utils.cut_hashtlb task_set n
    | None -> task_set in
  let in_header_basename = (include_sens_act_filename !Options.main_node) in
  let in_header_file  = (create_and_get_gendir basename) ^"/"^ in_header_basename ^".h" in
  let in_header_ch = open_out in_header_file in
  let in_header_f = formatter_of_out_channel in_header_ch in
  fprintf in_header_f "@[<v 1>/* File generated by preludec_genwrapper %s */@," Gwoptions.version;
  fprintf in_header_f "#ifndef _%s_H@," in_header_basename;
  fprintf in_header_f "#define _%s_H@,@," in_header_basename;
  pp_sensact_tasks in_header_f task_set_cut false;
  fprintf in_header_f "@]@,#endif@,@.";
  close_out in_header_ch

(** print the sensor and actuator node body *)
let pp_sensor_actuator_body task_set basename =
  let task_set_cut =
    match task_set_bound with
    | Some n -> Utils.cut_hashtlb task_set n
    | None -> task_set in
  let in_body_basename = include_sens_act_filename !Options.main_node in
  let in_body_file  = (create_and_get_gendir basename) ^"/"^ in_body_basename ^".c" in
  let in_body_ch = open_out in_body_file in
  let in_body_f = formatter_of_out_channel in_body_ch in
  fprintf in_body_f "@[<v 1>/* File generated by preludec_genwrapper %s */@," Gwoptions.version;
  pp_sensact_tasks in_body_f task_set_cut true;
  fprintf in_body_f "@]@,@.";
  close_out in_body_ch
