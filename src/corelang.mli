(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** The core language and its AST. Every element of the AST contains its
    location in the program text. The type and clock of an AST element
    is mutable (and initialized to dummy values). This avoids to have to
    duplicate AST structures (e.g. AST, typed_AST, clocked_AST). *)

type ident = Utils.ident
type rat = Utils.rat
type tag = Utils.tag

type atom =
  | AConst_int of int
  | AConst_char of char
  | AConst_float of float
  | AConst_bool of bool
  | AIdent of ident

type constant =
  | Const_int of int
  | Const_char of char
  | Const_float of float
  | Const_bool of bool

type type_dec =
    {ty_dec_desc: type_dec_desc;
     ty_dec_loc: Location.t}

and type_dec_desc =
  | Tydec_any
  | Tydec_int
  | Tydec_char
  | Tydec_float
  | Tydec_bool
  | Tydec_array of type_dec * int

type deadline_dec = int option

type clock_dec =
    {ck_dec_desc: clock_dec_desc;
     ck_dec_loc: Location.t}

and clock_dec_desc =
  | Ckdec_any
  | Ckdec_pclock of int * rat

type var_decl = 
    {var_id: ident;
     var_dec_type: type_dec;
     var_dec_clock: clock_dec;
     var_dec_deadline: deadline_dec;
     mutable var_type: Types.type_expr;
     mutable var_clock: Clocks.clock_expr;
     var_loc: Location.t;
     (** Order is used to retrieve var order at task set creation *)
     mutable var_order: int}

(** The tag of an expression is a unique identifier used to distinguish
    different instances of the same node *)
type expr =
    {expr_tag: tag; (** Unique identifier *)
     expr_desc: expr_desc;
     mutable expr_type: Types.type_expr;
     mutable expr_clock: Clocks.clock_expr;
     expr_loc: Location.t}

and expr_desc =
  | Expr_atom of atom
  | Expr_const of constant
  | Expr_ident of ident
  | Expr_tuple of expr list
  | Expr_fby of atom * expr
  | Expr_concat of atom * expr
  | Expr_tail of expr
  | Expr_when of expr * ident
  | Expr_whennot of expr * ident
  | Expr_merge of ident * expr * expr
  | Expr_appl of ident * expr
  | Expr_uclock of expr * int
  | Expr_dclock of expr * int
  | Expr_phclock of expr * rat
  | Expr_rate of expr * clock_dec
  | Expr_wcetlt of ident * expr * int
  | Expr_map of ident * int * expr

type eq =
    {eq_lhs: ident list;
     eq_rhs: expr;
     eq_loc: Location.t}

type node_desc =
    {node_id: ident;
     mutable node_type: Types.type_expr;
     mutable node_clock: Clocks.clock_expr;
     node_inputs: var_decl list;
     node_outputs: var_decl list;
     node_locals: var_decl list;
     node_eqs: eq list}

type imported_node_desc =
    {nodei_id: ident;
     mutable nodei_type: Types.type_expr;
     mutable nodei_clock: Clocks.clock_expr;
     nodei_inputs: var_decl list;
     nodei_outputs: var_decl list;
     nodei_wcet: int}

type sensor_desc =
    {sensor_id: ident;
     sensor_wcet: int}

type actuator_desc =
    {actuator_id: ident;
     actuator_wcet: int}

type const_desc =
    {const_id: ident;
     const_value: constant}

type top_decl_desc =
  | Node of node_desc
  | ImportedNode of imported_node_desc
  | SensorDecl of sensor_desc
  | ActuatorDecl of actuator_desc
  | ConstDecl of const_desc
        (** For predefined operators that are compiled into a task, ie merge and wcet operator *)

type top_decl =
    {top_decl_desc: top_decl_desc;
     top_decl_loc: Location.t}

type program = top_decl list

type assoc_program = (ident * top_decl) list * (ident * top_decl) list

type error =
    Main_not_found
  | Main_wrong_kind
  | No_main_specified

exception Error of error
exception Unbound_type of ident*Location.t

val prog_init : (ident * top_decl) list * (ident * top_decl) list

val pp_error : error -> unit

(** Caution, returns an untyped, unclocked, etc, expression *)
val expr_of_ident : ident -> Location.t -> expr

val expr_list_of_expr : expr -> expr list

val new_tag : unit -> tag

val numerate_vars : var_decl list -> var_decl list

val pp_prog_type : program -> unit

val pp_prog_clock : program -> unit

val get_IN_list : program -> program

val string_of_constant : constant -> string

val string_of_atom : atom -> string

val is_atom_zero : atom -> bool
