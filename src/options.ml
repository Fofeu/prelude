(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

let version = "%%PACKAGE_VERSION%%"
let main_node = ref ""
let print_types = ref false
let print_clocks = ref false
let print_deadlines = ref false
let print_protocols = ref false
let print_tasks = ref false
let print_tasks_simple_precs = ref false
let no_encoding = ref true
let export_protocols = ref true
let extern_buffers = ref false
let dest_dir = ref ""
let tracing = ref "no"
let real_is_double = ref true
let bool_is_stdbool = ref true
let aer_format = ref false
let aer_wcc = ref false
let io_code = ref true
let flat_precedence_set = ref true
let sorted_precedence_set = ref false

let configure_precedence_set s =
  match s with
  | "flat" ->
    begin
      flat_precedence_set := true;
      sorted_precedence_set := false;
    end
  | "sorted-by-task" ->
    begin
      flat_precedence_set := false;
      sorted_precedence_set := true;
    end
  | "all" ->
    begin
      flat_precedence_set := true;
      sorted_precedence_set := true;
    end
  | _ -> failwith ("Unknown precedence set configuration "^s)

let options =
  [ "-aer", Arg.Set aer_format, "produce C code following the AER format";
    "-aer_wcc", Arg.Unit (fun () -> aer_wcc := true; aer_format := true; bool_is_stdbool := false), "produce AER code for wcc";
    "-bool_is_int", Arg.Clear bool_is_stdbool, "map Prelude bool type to int (default is to use bool from C99)";
    "-cheddar", Arg.Set print_tasks, "simple formatting of task set for Cheddar tests (alias for print_tasks)";
    "-d", Arg.Set_string dest_dir, "produces code in the specified directory";
    "-extern_buffers", Arg.Set extern_buffers, "produces C code for external buffer allocation";
    "-precedence_set", Arg.Symbol(["flat"; "sorted-by-task"; "all"], configure_precedence_set), "configure how the precedence set should be exported" ;
    "-no_export_protocols", Arg.Clear export_protocols, "do not export communication protocols in generated code";
    "-no_io_code", Arg.Clear io_code, "produce no code for sensors/actuators. WARNING: only works combined with aer option";
    "-node", Arg.Set_string main_node, "specifies the main node";
    "-with_encoding", Arg.Clear no_encoding,
    "produces C code with RT attributes adjustment";
    "-print_types", Arg.Set print_types, "print node types";
    "-print_clocks", Arg.Set print_clocks, "print node clocks";
    "-print_deadlines", Arg.Set print_deadlines, "print node deadlines";
    "-print_protocols", Arg.Set print_protocols, "print communication protocols";
    "-print_tasks", Arg.Set print_tasks, "simple output of the compiled task set";
    "-print_tasks_simple_precs", Arg.Unit (fun () ->  print_tasks:= true; print_tasks_simple_precs:= true; no_encoding := true), "output of the task set and of simple precedences only";
    "-real_is_double", Arg.Set real_is_double, "map Prelude real type to double (WILL BE SUPPRESSED-THIS IS THE DEFAULT NOW)";
    "-real_is_float", Arg.Clear real_is_double, "map Prelude real type to float (default is double)";
    "-tracing", Arg.Set_string tracing, "output trace format (allowed values: no, values, instances, lttng-values, lttng-instances)";
    "-version", Arg.Unit (fun () -> print_endline version), " Display the version";]

type trace_fmt_type =
    No
  | Values
  | Instances
  | LTTngValues
  | LTTngInstances

let trace_fmt () =
  match !tracing with
  | "values" -> Values
  | "instances" -> Instances
  | "lttng-values" -> LTTngValues
  | "lttng-instances" -> LTTngInstances
  | "no" -> No
  | _ -> failwith ("Unknown trace format " ^ !tracing)

let trace_enabled () =
  trace_fmt () <> No
let trace_values () =
  trace_fmt () = Values || trace_fmt () = LTTngValues
let trace_instances () =
  trace_fmt () = Instances || trace_fmt () = LTTngInstances
let trace_printf () =
  trace_fmt () = Values || trace_fmt () = Instances
let trace_lttng () =
  trace_fmt () = LTTngValues || trace_fmt () = LTTngInstances
