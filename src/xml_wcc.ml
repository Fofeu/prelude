(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generate xml file describing the task set for wcc. *)

open Task_set
open C_utils
open Format

(** Generate declaration of task [tname] *)
let pp_task t out_f =
  if t.task_kind <> CstTask then
    begin
      let period = t.task_period in
      let needs_idx, needs_sidx = needs_idxs t in
      if (t.task_kind <> CstTask) && (needs_idx || needs_sidx) then
        begin
          fprintf out_f  "\t\t\t<entrypoint>@.";
          fprintf out_f  "\t\t\t<function>%s</function>@." (fun_name_init t.task_id);
          fprintf out_f  "\t\t\t</entrypoint>@."
        end;
      if (t.task_kind <> Sensor) then
          begin
            fprintf out_f  "\t\t\t<entrypoint>@.";
            fprintf out_f  "\t\t\t<function>%s</function>@." (fun_name_A t.task_id);
            fprintf out_f  "\t\t\t<period>%d</period>@." period;
            fprintf out_f  "\t\t\t</entrypoint>@.";
          end;
      fprintf out_f  "\t\t\t<entrypoint>@.";
      fprintf out_f  "\t\t\t<function>%s</function>@." (fun_name_E t.task_id);
      fprintf out_f  "\t\t\t<period>%d</period>@." period;
      fprintf out_f  "\t\t\t</entrypoint>@.";
      if (t.task_kind <> Actuator) then
          begin
            fprintf out_f  "\t\t\t<entrypoint>@.";
            fprintf out_f  "\t\t\t<function>%s</function>@." (fun_name_R t.task_id);
            fprintf out_f  "\t\t\t<period>%d</period>@." period;
            fprintf out_f  "\t\t\t</entrypoint>@."
          end
    end

(** Generate all task declarations for [task_set]. *)
let pp_tasks task_set out_f =
    Hashtbl.iter
    (fun _ t ->
      pp_task t out_f)
    task_set

(** Generate the xml file for [task_set]. *)
let pp_prog task_set basename =
  let nfile  = (create_and_get_gendir basename) ^"/"^ !Options.main_node^".tasks" in
  let name_ch = open_out nfile in
  let out_f = formatter_of_out_channel name_ch in
  let extbuf_file = (extbuffers_filename !Options.main_node)^".c" in
  let c_file = !Options.main_node ^".c" in
  let inc_file =  (includes_filename !Options.main_node) ^".c" in
  fprintf out_f "<task>@.";
  fprintf out_f "\t<name>tache1</name>@.";
  fprintf out_f "\t<core>0</core>@.";
  fprintf out_f "\t<sources>@.";
  fprintf out_f  "\t\t<file>main.c</file>@.";
  fprintf out_f  "\t\t<file>%s</file>@." extbuf_file;
  fprintf out_f  "\t\t<file>manual.c</file>@.";

  fprintf out_f  "\t\t<file>%s</file>\n" inc_file;
  if !Options.io_code then
    begin
      let inc_sens_act_file = (include_sens_act_filename !Options.main_node) ^".c" in
      fprintf out_f  "\t\t<file>%s</file>\n" inc_sens_act_file
    end;
  fprintf out_f  "\t\t<file>%s\n" c_file;

  let task_set_cut = filter_io task_set in

  pp_tasks task_set_cut out_f;

  fprintf out_f  "\t\t</file>\n";

  fprintf out_f "\t</sources>@.";
  fprintf out_f "\t<wccrc>.wccrc</wccrc>@.";
  fprintf out_f "</task>@.";
  fprintf out_f "@.";
  close_out name_ch
