\documentclass{beamer}

\usepackage{pgfpages}
\usepackage{tikz}
\usetheme{progressbar}
\usepackage{times}
\usepackage{listings}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{mycommands}
\usepackage{abbrevs-gb}
\usepackage{url}

\usetikzlibrary{positioning}
\usetikzlibrary{shapes.misc}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{chains}
\usetikzlibrary{arrows}
\usetikzlibrary{decorations.pathreplacing}
%\pgfpagesuselayout{4 on 1}[a4paper,border shrink=5mm,landscape]

\lstset{basicstyle=\ttfamily}

\title{Logical time and real-time in the Synchronous approach}
\author{Julien Forget\\LIFL - Universit\'e Lille 1}
\date{}

\begin{document}
\frame{\titlepage}

\AtBeginSection
{
  \begin{frame}
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{frame}{Overview}
  At the end of this session you should understand:
  \begin{itemize}
  \item Why introducing explicit real-time constraints in a  synchronous
    language is useful;
  \item How we can deal with \emph{both} logical-time and real-time;
  \item The implications of the introduction of real-time in the
    language structure and compilation.
  \end{itemize}

  % Globalement : ajouter des refs + exos/questions ouvertes ?

  % Extensions possibles : clock calculus, ordo, ordo à la main -> en TD ? TP :
  % chrono, clock calculus ?
\end{frame}

\section{Real-time}

\begin{frame}{Reactive system (reminder)}
    \begin{center}
    \input{Figures/control-system-fig.tex}
  \end{center}
  
  \begin{itemize}
  \item React to inputs:
    \begin{enumerate}
    \item Acquire inputs on sensors;
    \item Compute;
    \item Produce values on actuators.
    \end{enumerate}
  \item Actions impact the environment, thus subsequent inputs;
  \item Response time must be \emph{bounded}, due to environment
    evolving autonomously.
  \end{itemize}
\end{frame}

\begin{frame}{Real-time system}
  \begin{definition}
    Real-time systems must guarantee response within strict time
    constraints, often referred to as ``deadlines''.\\ (Wikipedia)
  \end{definition}
  \begin{itemize}
  \item Similar to reactive systems;
  \item \emph{Several, predefined time bounds}.
  \end{itemize}
\end{frame}

\begin{frame}{Example: UAV control}
  \begin{center}
    \includegraphics[scale=.5]{Figures/UAV.jpg}
  \end{center}

  Real-time constraints:
  \begin{itemize}
  \item GPS (input): 1 frame every 250 ms.
    \begin{itemize}
    \item Deadline miss $\Rightarrow$ frame lost (current position),
      wrong trajectory.
    \end{itemize}
  \item Attitude regulation (output): consolidate actuator orders every 60ms
    \begin{itemize}
    \item Deadline miss $\Rightarrow$ loss of control.
    \end{itemize}
  \item Failure detection (internal): check inconsistencies every 200ms
    \begin{itemize}
    \item Deadline miss $\Rightarrow$ crash with motors on.
    \end{itemize}
  \item ...
  \end{itemize}
\end{frame}

\begin{frame}{Classic model}
  Program=a set of tasks (threads) $\tau_i$:

\bigskip

  \begin{minipage}[c]{.46\linewidth}
    \include{Figures/task-model}
  \end{minipage}\hfill
  \begin{minipage}[c]{.46\linewidth}
    \begin{itemize}
    \item $T_i$: period;
    \item $D_i$: relative deadline ($D_i<=T_i$);
    \item $C_i$: worst-case execution time (WCET);
    \item $O_i$: initial release date;
    \item $\tau_{i.p}$: $p^{th}$ job of $\tau_i$.
    \end{itemize}
  \end{minipage}
\end{frame}

\begin{frame}{Deadlines and periods}
  \begin{itemize}
  \item \emph{Deadline}: respond before some specified time;
  \item \emph{Period}: processes are recurring at regular time intervals;
  \item The period is often an implicit deadline (non-reentrant tasks);
  \item Choice of the periods/deadlines:
    \begin{itemize}
    \item Lower-bound: physical constraints of the sensors/actuators;
    \item Lower-bound: computation time;
    \item Upper-bound: too slow can lead to an unsteady system.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Execution times}
  \begin{itemize}
  \item Evaluating the execution time of some process is {\large HARD}
    \begin{itemize}
    \item Depends on the content of the memory;
    \item Depends on the content of the pipeline;
    \item Depends on the values processed;
    \item Other processes may interfere;
    \item OS may interfere...
    \end{itemize}
  \item Validating temporal behaviour with variable execution times is complex;
  \item[$\Rightarrow$] Execution times are (largely) over-evaluated by a
    \emph{Worst-Case Execution Time} (WCET).
  \end{itemize}

\end{frame}

\begin{frame}{Real-time multi-tasking}
  Some classic problems:
  \begin{itemize}
  \item \emph{Scheduling policy}: define an algorithm that finds an
    execution order (a schedule), that respects all deadlines;
  \item \emph{Schedulability analysis}: ensure before execution that deadlines
    can and will be met (for a given policy);
  \item Data-dependencies  $\Rightarrow$ scheduling
    policy for dependent tasks + synchronization primitives
    (e.g. semaphores, buffers, \ldots);
  \item Shared resources $\Rightarrow$ problems similar to communication synchronizations.
  \end{itemize}
\end{frame}

\begin{frame}{Scheduling: multi-processor example}
  $\tau_B(T_B=9,C_B=5)$ and $\tau_A(T_A=3,C_A=1)$:
  
  \include{Figures/exec-multiproc}
  
\end{frame}

\begin{frame}{Scheduling: mono-processor example}
  $\tau_B(T_B=9,C_B=5)$ and $\tau_A(T_A=3,C_A=1)$:
  
  \begin{itemize}
  \item Without preemption:
    \include{Figures/deadline-miss}
  \item With preemption:
    \include{Figures/preemption}
  \end{itemize}
  
\end{frame}

\begin{frame}{Scheduling policy example: Rate-Monotonic}
  \begin{itemize}
  \item Fixed-task priorities: a fixed priority is assigned to each task;
  \item Task with smaller relative deadline (=period) gets a higher priority;
  \item Works only when $D_i=T_i$;
  \item This policy is \emph{optimal} among the fixed-task priority
    policies.\\
    \pause $\Rightarrow$ What does \emph{optimal} mean ?
  \end{itemize}
  
\end{frame}

\begin{frame}{Rate-Monotonic analysis}

  \emph{Sufficient} schedulability test:
  \[\sum_{i=0}^m\frac{C_i}{T_i}\leq m(2^{1/m}-1)\]

  $\simeq 0.8$ for $m=2$ and tends towards 0.7 for big $m$.

  \pause
  $\Rightarrow$ What does \emph{sufficient} mean ?

  \vspace{1em}

  \pause
  \emph{NB}: More general cases ($D_i\leq T_i$, multi-core, ...) are
  in many cases NP.

\end{frame}

\begin{frame}{Okay...}
  \pause
    {\Large But, we were told to ignore real-time !}\\
    (cf \includegraphics[scale=.1]{Figures/chand1.jpg} )

\end{frame}

\begin{frame}{Yet, knowing real-time constraints is useful}

  Based on real-time constraints we can:
  \pause
  \begin{itemize}
  \item Schedule better:
    \begin{itemize}
    \item Optimize processor utilization (do not execute tasks more
      frequently than required);
    \item Ensure temporal correction by assigning priorities based on
      deadlines.
    \end{itemize}
    \pause
  \item Statically analyze the real-time behaviour: check before execution
    that the system will not become overloaded/late;
    \pause
  \item As a side effect, this also enables a better dimensioning of the
    hardware platform.
  \end{itemize}
\end{frame}

\begin{frame}{So...}
  \begin{center}
    \includegraphics[scale=.5]{Figures/chandelier_broken.jpg}

    Did we break it ?

    \vspace{1em}
    \pause
    No, but we need more to cover the development cycle.
  \end{center}
\end{frame}

\section{Multi-rate system design}

\begin{frame}{Programming in the large: Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle1.jpg}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle2.jpg}
\end{frame}

\begin{frame}{Aircraft functions}
  Example:
  \begin{itemize}
  \item Thruster control;
  \item Flight plan control;
  \item Aircraft control on ground;
    \begin{itemize}
    \item Transition air/ground;
    \item \emph{Deceleration};
    \item Direction control on ground;
    \item \ldots
    \end{itemize}
  \item \ldots
  \end{itemize}
\end{frame}


\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle3.jpg}
\end{frame}

\begin{frame}{Aircraft systems}
  Example: \emph{Ground deceleration} is made up of:
  \begin{itemize}
  \item The ``thrust reversal'' function of the \emph{motor control} system;
  \item The ``spoiler control'' function of the \emph{flight command} system;
  \item The \emph{wheel brake} system.
  \end{itemize}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle4.jpg}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle5.jpg}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle6.jpg}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle7.jpg}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle8.jpg}
\end{frame}

\begin{frame}{Aeronautics system design}
  \includegraphics[width=.9\linewidth]{Figures/lifecycle9.jpg}
\end{frame}

\begin{frame}{Synchronous languages in the design}
  \pause
  \begin{itemize}
  \item On the ``system'' level:
    \pause
    \begin{itemize}
    \item Functional level (\scade, \lustre);
      \pause
    \item Software architecture level ?
    \end{itemize}
    \pause
  \item Timing requirements: \pause
    \begin{itemize}
    \item Attached to blocks (software architecture);
    \item \emph{Abstracted} on functional level: blocks are
      mono-periodic.
    \end{itemize}
  \end{itemize}
  
  \pause $\Rightarrow$ Can we introduce the synchronous paradigm at the
  software architecture level and \alert{deal with timing requirements there ?}
\end{frame}

\section{Synchronous real-time}

\begin{frame}{Synchronous approach (reminder)}
    Real-time is replaced by a simplified, abstract, logical time.

  \begin{itemize}
  \item Instant: one reaction of the system;
  \item \emph{Logical time}: sequence of instants;
  \item The program describes what happens at each instant;
  \item Synchronous hypothesis: \emph{computations complete before the
    next instant}. If so:
    \begin{itemize}
    \item[$\Rightarrow$] We can ignore time inside an instant, only the order matters;
    \item[$\Rightarrow$] We are only interested in how instants are chained together.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{A question of semantics}

  \begin{itemize}
  \item Zero-time ?
    \begin{itemize}
    \item In the semantics, the execution of one instant takes no time,
      everything happens simultaneously;
    \item When implemented, the execution of one instant \emph{does take
      time};
    \item The point is, when writing a synchronous program, we
      do not care about real-time.
    \end{itemize}
    \pause
  \item Synchronous \alert{hypothesis} validation:
    \begin{itemize}
    \item In aeronautics design (and in many other cases), the
      periodicity of a block (\lustre\ program) sets the bound for the
      duration of an instant;
    \item At the end of the implementation process, the synchronous
      hypothesis must be validated, i.e. ``do we
      have $C_i \leq T_i$ ?'' (WCET analysis)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Multi-rate in \lustre/\scade}
  \begin{overlayarea}{\textwidth}{3cm}
    \begin{onlyenv}<1|handout:1>
  \begin{example}
    \begin{center}
      \begin{tikzpicture}[scale=.5,font=\scriptsize]
        \draw (0,0) rectangle node[above=5pt] {$period=10ms$} (2,1)  node[pos=0.5]{F};
        \draw[-latex] (2,0.8) -- (4,0.8);
        \draw[-latex] (4,0.2) -- (2,0.2);
        \draw (4,0) rectangle node[below=5pt] {$period=30ms$}(6,1) node[pos=0.5]{S};
        \draw[-latex] (-1,0.8) -- (0,0.8);
        \draw[-latex] (0,0.2) -- (-1,0.2) node[left] {$8ms >$};
      \end{tikzpicture}
    \end{center}
  \end{example}
    \end{onlyenv}
    \begin{onlyenv}<2|handout:2>

\smallskip

  Behaviour:

\smallskip

  {\scriptsize
    \begin{tabular}{l|cccccccc}
      \hline
      \lstinline!vf! & $vf_0$ & $vf_1$ &  $vf_2$ & $vf_3$ & $vf_4$ &
      $vf_5$ & $vf_6$ & $\ldots$\\
      \hline
      \lstinline!vf when clock3! & $vf_0$ & & & $vf_3$ & & & $vf_6$ & $\ldots$\\
      \hline
      \lstinline!vs! & $vs_0$ &  &  & $vs_1$ &  & & $vs_2$ & $\ldots$\\
      \hline
      \lstinline!0 fby vs! & 0 &  &  & $vs_0$ &  & & $vs_1$ & $\ldots$\\
      \hline
      \lstinline!current (0 fby vs)! & 0 & 0 & 0 & $vs_0$ & $vs_0$ &
      $vs_0$ & $vs_1$ & \ldots\\
      \hline
    \end{tabular}
  }      
    \end{onlyenv}
  \end{overlayarea}

  \begin{block}{Program (base period=10ms)}
    \begin{center}
      \begin{lstlisting}[basicstyle=\scriptsize]
node multi_rate(i: int) returns (o: int)
var vf: int; clock3: bool; vs: int when clock3;
let
  (o, vf)=F(i, current(0 fby vs));
  clock3=everyN(3);
  vs=S(vf when clock3);
tel
      \end{lstlisting}
    \end{center}
  \end{block}
\end{frame}

\begin{frame}{What's missing ?}

  \begin{itemize}
  \item \emph{For the programmer}: not immediate to see that
    \lstinline!vf when clock3! is 3 times slower than \lstinline!vf!;
  \item \emph{For the static analyses}: clocks = Boolean expressions $\Rightarrow$
    compiler does not see that "some clock is 3 times slower than another";
  \item \emph{For the code generation}: computations must all complete
    during one base period (10ms).
  \end{itemize}
\end{frame}

\begin{frame}{Objective: multi-Rate Synchronous}
   \begin{tikzpicture}
     \draw[->] (0,0) -- (5.9,0) node[anchor=west]{time};
     \foreach \x/\xx in {0/0.4,1/1.4,2/2.4,3/3.4,4/4.4,5/5.4}
     \draw[font=\tiny] (\x cm,0.5 cm) rectangle (\xx cm,1 cm) node[pos=0.5]{F};
     \foreach \x/\xx in {0/1.8,3/4.8}
     \draw[font=\tiny] (\x cm,1.5 cm) rectangle (\xx cm,2 cm) node[pos=0.5]{S};
     \foreach \x in {0,...,5}
     \draw[dotted,blue] (\x cm, -0.3 cm) -- (\x cm,2.5cm);
     \draw[<->,blue,font=\scriptsize] (0,-0.2) node[below
     right=2pt]{Scale 2: fast instants (10ms)} -- (1,-0.2);
     \draw[<->,blue,font=\scriptsize] (0,2.3) node[above
     right=2pt]{Scale 1: slow instants (30ms)} -- (3,2.3);
   \end{tikzpicture}
 
   \smallskip

  Requirements:
  \begin{itemize}
  \item Define several logical time scales;
  \item Compare different logical time scales;
  \item Transition from one scale to another.
  \end{itemize}

\end{frame}

\begin{frame}{Bridging the gap}
  Main ideas:
  \begin{itemize}
  \item \emph{Arithmetic clocks}: clocks defined, compared and transformed,
    using \alert{numbers} and/or operations on numbers;
  \item \emph{Multi-threaded execution}: not all operations must be executed within
    the same base period.
  \end{itemize}
\end{frame}

\subsection{Arithmetic clocks}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{N-Synchronous}
  \begin{itemize}
  \item \emph{Motivation}: implementing real-time streaming applications
    (e.g. video systems);
    \begin{itemize}
    \item Multi-rate systems;
    \item Combine flows that are ``nearly synchronous'', i.e. the
      same production rate on a period of time, but not at the same
      instants.
    \end{itemize}
  \item Compiled into classic synchronous code + buffering mechanisms.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{N-Synchronous (2)}
  \begin{example}
  \lstinputlisting[basicstyle=\scriptsize]{Figures/n-sync.luc}
\end{example}

\bigskip
\begin{overlayarea}{\textwidth}{10cm}
  
  \begin{onlyenv}<1|handout:1>
Operators
  \begin{itemize}
  \item \lstinline!x when (01)!: drop value, keep value,
    drop value, keep value, \ldots;
  \item \lstinline!buffer(x1)!: buffer values to enable clock ``resynchronization''.
  \end{itemize}
\end{onlyenv}

\begin{onlyenv}<2|handout:2>
{\scriptsize
  \begin{tabular}{l|ccccccc|c}
    flow & & & & & & & & clock\\
    \hline
    \lstinline!x! & 5 & 7 & 3 & 6 & 2 & 8& \ldots & (1)\\
    \hline
    \lstinline!x1! & 5 & & 3 & & 2 & & \ldots & (10) \\
    \hline
    \lstinline!buffer(x1)! & & 5 & & 3 & & 2 & \ldots & (01)\\
    \hline
    \lstinline!x2! & & 7 & & 6 & & 8 & \ldots & (01) \\
    \hline
    \lstinline!o! & & 12 & & 9 & & 10 & \ldots & (01) \\
    \hline
  \end{tabular}
}
\end{onlyenv}
\end{overlayarea}
\end{frame}

\begin{frame}{N-Synchronous (3)}
  \begin{itemize}
  \item Rate relations are more explicit;
  \item Better static analyses;
  \item More general (too general ?) than purely multi-periodic systems (e.g. clock (10110));
  \item Semantics still requires computations to fit within an instant.
  \end{itemize}
\end{frame}


\begin{frame}{CCSL}
  (Presented previously by AG).
  \begin{itemize}
  \item Very expressive: periodic, sampled, alternation, etc;
  \item Targeted mainly for simulation/verification;
  \item Too general for efficient compilation (?)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\PCLOCKS}

  \begin{itemize}
  \item Definition: Clock $(n,p)$ is a clock of period $n$ and phase
    $p$;
  \item Example: $(120,1/2)$ activates at \emph{dates} 60, 180,
    300, 420, \ldots
  \item Rate transformations:
    \begin{itemize}
    \item $\alpha\Each k$: divide frequency;
    \item $\alpha\Times k$: multiply frequency;
    \item $\alpha\Phase q$: offset activations.
    \end{itemize}  
  \end{itemize}
  
  \begin{center}
    \input{Figures/pclock-trans-fig}
  \end{center}
  
\end{frame}

\begin{frame}{\PCLOCKS (2)}
  \begin{itemize}
  \item \Pclocks\ are dedicated to multi-periodic real-time systems;
  \item \Pclocks\ are a sub-class of Boolean clocks and of N-Synchronous
    clocks;
  \item \alert{This restriction enables to compile real-time aspects
    more efficiently.}
  \end{itemize}
  
\end{frame}

\subsection{Multi-threaded execution}

\begin{frame}{Outline}
    \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{Relaxed Synchronous hypothesis}
  \begin{block}{Classic Synchronous hypothesis}
    All computations complete before the end of the instant.
  \end{block}

  \begin{block}{Relaxed Synchronous hypothesis}
    Computations complete before their next activation.
  \end{block}

  \begin{itemize}
  \item Relaxed: mere reformulation of classic;
  \item Classic: particular case of relaxed;
  \item Relaxed: supports several logical time scales;
  \item Relaxed: \emph{fits with periodicity constraints} ``a task
    instance must complete before the next task release''.
  \end{itemize}
\end{frame}

\begin{frame}{Automated code distribution into threads}
  (Presented previously by AG-not the same).

\bigskip

  \emph{Approach 1}: Automatically split the code into several threads:
  \begin{itemize}
  \item In Signal: split code based on clocks;
  \item In Lustre: split code based on inputs/outputs;
  \item Add buffers to communicate between threads.
  \end{itemize}


\end{frame}

\begin{frame}{Automated code distribution into threads (2)}
  More general than periodic systems, thus:
  \begin{itemize}
  \item Buffer dimensioning is harder;
  \item Temporal analyses is harder;
  \item The user must specify the distribution criteria.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Lustre with Futures}
  \emph{Approach 2}: Explicit thread encapsulation.
  \begin{example}
  \lstinputlisting[basicstyle=\scriptsize]{Figures/future.luc}
\end{example}

\smallskip
\begin{overlayarea}{\textwidth}{10cm}
  
  \begin{onlyenv}<1|handout:1>

  \begin{itemize}
  \item \lstinline!async! encapsulates a node inside a thread;
  \item The value of an asynchronous flow is fetched using operator
    \lstinline+!+.
  \item[NB] The values and clocks of \lstinline+!x+ and \lstinline!x! are exactly the same.
  \end{itemize}
\end{onlyenv}

\begin{onlyenv}<2|handout:2>
{\scriptsize
  \begin{tabular}{l|cccccc}
    \hline
    \lstinline!big! & true & false & false & true  & false & \ldots \\
    \hline
    \lstinline+!ys+ & 0.0 &  &  & 3.14  &  & \ldots \\
    \hline
    \lstinline!yf! &  & 1.0 & 2.0 & & 4.14 & \ldots \\
    \hline
    \lstinline!y! & 0.0 & 1.0 & 2.0 & 3.14 & 4.14 &\ldots\\
    \hline
    \lstinline!v! & 0.0 & 0.0 & 1.0 & 2.0 & 3.14 & \ldots \\
    \hline
  \end{tabular}
}
\end{onlyenv}
\end{overlayarea}
\end{frame}

\begin{frame}{Lustre with Futures (2)}
  \begin{itemize}
  \item Good multi-thread support;
  \item No real-time constraints attached to threads.
  \end{itemize}
\end{frame}

\begin{frame}{Prelude}
  \emph{Approach 3}: Thread assembly language.
  \begin{itemize}
  \item Each node invocation is encapsulated inside a thread;
  \item Targeted for the software architecture level;
  \item Real-time characteristics are associated to each node/thread.
  \end{itemize}
\end{frame}

\section{\prelude}

\begin{frame}{Prelude: a real-time synchronous language}
  \begin{itemize}
  \item \emph{Initial question}: how to program systems with multiple
    real-time constraints in a synchronous style ?
  \item \emph{Context}:
    \begin{itemize}
    \item Defined and developed at ONERA (first during speaker thesis);
    \item Motivated by collaborations with Airbus and Astrium (satellites).
    \end{itemize}
  \item \emph{Main principles}:
    \begin{itemize}
    \item \Pclocks;
    \item Relaxed synchronous hypothesis;
    \item Fully multi-threaded;
    \item At the software architecture level.
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{The language}

\begin{frame}
  \frametitle{Operations}
  \begin{block}{Multi-rate system}
    \begin{center}
      \begin{tikzpicture}[scale=.5,font=\scriptsize]
        \draw[thick,blue] (0,0) rectangle node[above=5pt,black] {$period=10ms$} (2,1)  node[pos=0.5,blue]{F};
        \draw[-latex] (2,0.8) -- (4,0.8);
        \draw[-latex] (4,0.2) -- (2,0.2);
        \draw[thick,blue] (4,0) rectangle node[below=5pt,black] {$period=30ms$}(6,1) node[pos=0.5,blue]{S};
        \draw[-latex] (-1,0.8) -- (0,0.8);
        \draw[-latex] (0,0.2) -- (-1,0.2) node[left] {$8ms >$};
      \end{tikzpicture}
    \end{center}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Operations: imported nodes}
  \begin{itemize}
  \item Operations of the system are imported nodes;
  \item External functions (e.g. \C, or \lustre);
  \item Declare the worst case execution time (wcet) of the node.
  \end{itemize}
  \begin{example}
  \begin{lstlisting}[basicstyle=\scriptsize]
imported node F(i, j: int) returns (o, p: int) wcet 2;
imported node S(i: int) returns (o: int) wcet 10;
  \end{lstlisting}
\end{example}
\end{frame}

\begin{frame}
  \frametitle{Real-time constraints}
  \begin{block}{Multi-rate system}
    \begin{center}
      \begin{tikzpicture}[scale=.5,font=\scriptsize]
        \draw (0,0) rectangle node[above=5pt,blue] {$period=10ms$} (2,1)  node[pos=0.5]{F};
        \draw[-latex] (2,0.8) -- (4,0.8);
        \draw[-latex] (4,0.2) -- (2,0.2);
        \draw (4,0) rectangle node[below=5pt,blue] {$period=30ms$}(6,1) node[pos=0.5]{S};
        \draw[-latex] (-1,0.8) -- (0,0.8);
        \draw[-latex] (0,0.2) -- (-1,0.2) node[left,blue] {$8ms >$};
      \end{tikzpicture}
    \end{center}
  \end{block}

\smallskip

\end{frame}

\begin{frame}[fragile]
  \frametitle{Real-time constraints: clocks and deadlines}
  \begin{itemize}
  \item Real-time constraints are specified in the signature of a node;
  \item Periodicity constraints on inputs/outputs;
  \item Deadline constraints on inputs/outputs.
  \end{itemize}

  \begin{example}    
  \begin{lstlisting}[basicstyle=\scriptsize]
node sampling(i: rate (10,0)) returns (o: rate (10,0) due 8)
let
  ...
tel
  \end{lstlisting}
  \end{example}

  Input/output rate can be unspecified, the compiler will infer it.
\end{frame}

\begin{frame}
  \frametitle{Multi-rate communications}
  \begin{block}{Multi-rate system}
    \begin{center}
      \begin{tikzpicture}[scale=.5,font=\scriptsize]
        \draw (0,0) rectangle node[above=5pt] {$period=10ms$} (2,1)  node[pos=0.5]{F};
        \draw[-latex,thick,blue] (2,0.8) -- (4,0.8);
        \draw[-latex,thick,blue] (4,0.2) -- (2,0.2);
        \draw (4,0) rectangle node[below=5pt] {$period=30ms$}(6,1) node[pos=0.5]{S};
        \draw[-latex] (-1,0.8) -- (0,0.8);
        \draw[-latex] (0,0.2) -- (-1,0.2) node[left] {$8ms >$};
      \end{tikzpicture}
    \end{center}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Multi-rate communications: rate transition operators}
  \begin{example}
  \begin{lstlisting}[basicstyle=\scriptsize]
node sampling(i: rate (10, 0)) returns (o)
  var vf, vs;
let
  (o, vf)=F(i, (0 fby vs)*^3);
  vs=S(vf/^3);
tel
  \end{lstlisting}
\end{example}

\bigskip
\begin{overlayarea}{\textwidth}{10cm}
  
  \begin{onlyenv}<1|handout:1>
Rate transition operators:
  \begin{itemize}
  \item Sub-sampling: $x\Dclock 3$ ($\Clock{x}\Each 3$);
  \item Over-sampling: $x\Uclock 3$ ($\Clock{x}\Times 3$).
  \end{itemize}
\end{onlyenv}

\begin{onlyenv}<2|handout:2>
{\scriptsize
  \begin{tabular}{l|cccccccccc}
    \hline
    date & 0 & 10 & 20 & 30 & 40 & 50 & 60 & 70 & 80 & ...\\
    \hline
    \lstinline!vf! & $vf_0$ & $vf_1$ & $vf_2$ & $vf_3$ & $vf_4$ & $vf_5$ &
    $vf_6$ & $vf_7$ & $vf_8$ & ...\\
    \hline
    \lstinline!vf/^3! & $vf_0$ &  &  & $vf_3$ &  &  &
    $vf_6$ & & & ...\\
    \hline
    \lstinline!vs! & $vs_0$ &  &  & $vs_1$ &  &  &
    $vs_2$ & & & ...\\
    \hline
    \lstinline!0 fby vs! & $0$ &  &  & $vs_0$ &  &  &
    $vs_1$ & & & ...\\
    \hline
    \lstinline!(0 fby vs)*^3! & 0 & 0 & 0 & $vs_0$ & $vs_0$ &$vs_0$  &
    $vs_1$ &$vs_1$ &$vs_1$ & ...\\
    \hline
  \end{tabular}
}
\end{onlyenv}
\end{overlayarea}
\end{frame}

\begin{frame}{And...}
  \pause
  \begin{center}
    That's all folks !
  \end{center}
\end{frame}

\begin{frame}{Formal semantics: \PCLOCKS}

  \begin{itemize}
  \item Flow values are tagged by a date: $f=(v_i,t_i)_{i\in\mathbb{N}}$;
  \item Clock = sequence of tags of the flow;
  \item Value $v_i$ must be produced during time interval $[t_i,t_{i+1}[$;
  \item A clock is \emph{strictly periodic} iff:
    \[\exists n\in\mathbb{N}^{*},\;\forall i\in\mathbb{N},\;
    t_{i+1}-t_i=n\]
  \item $n$ is the period of $h$, $t_0$ is the phase of $h$.
  \item Eg: $\isp{120,1/2}$ is the clock of period 120 and phase 60.
  \end{itemize}

\end{frame}

\begin{frame}{Formal semantics: operators}
  \begin{example}
    $\Sharp{+}((v,t).s,(v',t).s')=(v+v',t).\Sharp{+}(s,s')$
  \end{example}


  \begin{itemize}
  \item $(v,t).s$: denotes value $v$ produced at time $t$ and followed
    by sequence $s$;
  \item $\Sharp{op}(f,f')=(v_1,t_1).(v_2,t_2)\ldots$ denotes the flow
    produced when applying $op$ to flows $f$ and $f'$.
  \end{itemize}

  \textbf{Warning}:
  \begin{itemize}
  \item The semantics is \emph{ill-defined for asynchronous flows};
  \item[$\Rightarrow$] Static analyses required to check that program
    semantics is well-defined before further compilation.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Formal semantics: classic operators}

  \begin{equation*}
    \Sharp{\Fby}(v,(v',t).s) =
    (v,t).\Sharp{\Fby}(v',s)
  \end{equation*}
  
  \begin{align*}
    \Sharp{\When}((v,t).s,(true,t).cs) &= (v,t).\Sharp{\When}(s,cs)\\
    \Sharp{\When}((v,t).s,(false,t).cs) &= \Sharp{\When}(s,cs)
  \end{align*}
  
\end{frame}

\begin{frame}
  \frametitle{Formal semantics: rate transitions}


  \begin{center}
    $\Sharp{\Uclock}((v,t).s,k) =
    \displaystyle
    \prod^{k-1}_{i=0}(v,t'_i).\Sharp{\Uclock}(s,k)$\\
    
    (\mbox{with } $t'_0 = t$ \mbox{ and } $t'_{i+1}-t'_i =\Period{s}/k$)
  \end{center}

  \smallskip

  \begin{center}
    $\Sharp{\Dclock}((v,t).s,k) = 
    \begin{cases}
      (v,t).\Sharp{\Dclock}(s,k) & \text{if }k*\Period{s}|t\\
      \Sharp{\Dclock}(s,k) & \text{otherwise}
    \end{cases}$
  \end{center}
\end{frame}

\subsection{Compilation}

\begin{frame}{Compilation overview}
  \begin{center}
    {\scriptsize
    \input{Figures/prelude-compil-overview}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Static analyses}
  \begin{itemize}
  \item Typing: no run-time type error;
  \item Causality analysis: no cyclic data-dependencies;
  \item Clock calculus: values are only accessed when they should
    be.
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Clock calculus: example}
  \begin{example}
    \lstinputlisting[basicstyle=\scriptsize]{Figures/poly.lrt}
  \end{example}
  \smallskip
  \begin{block}{Result inferred by the clock calculus}
    \begin{lstlisting}[basicstyle=\scriptsize]
under_sample: 'a->'a/.2
poly: ((10,0) * (5,0))->((20,0) * (10,0))
\end{lstlisting}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{Task graph extraction}
  \begin{block}{Program}
\lstinputlisting[basicstyle=\scriptsize]{Figures/simple-sampling.lrt}
  \end{block}

  \begin{block}{Task graph}
    \begin{center}
{\scriptsize
  \input{Figures/task-graph-fig}
}
    \end{center}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Real-time characteristics}

  \begin{center}
  \begin{tikzpicture}[font=\scriptsize]
    \draw[dashed] (0,0.8) -- (0,2) node[above] {$O_i$};
    \draw (0.1,1) rectangle (1.9,1.5) node[pos=0.5]{$C_i$};
    \draw[dashed] (2.4,1) -- (2.4,1.7);
    \draw[<->] (0,1.6) --  node[above] {$D_i$} (2.4,1.6);
    \draw[dashed] (3,0.8) -- (3,2);
    \draw (3.6,1) rectangle (5.4,1.5) node[pos=0.5]{$C_i$};
    \draw[dashed] (5.4,1) -- (5.4,1.7);
    \draw[<->] (3,1.6) --  node[above] {$D_i$} (5.4,1.6);
    \draw[dashed] (6,0.8) -- (6,2);
    \draw (-0.5,1) -- (-0.5,1.2) node[above] {0};
    \draw[->] (-0.5,1) -- (6.5,1);
    \draw[<->] (0,0.8) --  node[below] {$T_i$} (3,0.8);
    \draw[<->] (3,0.8) --  node[below] {$T_i$} (6,0.8);
  \end{tikzpicture}
  \end{center}

  For each task:
  \begin{itemize}
  \item Repetition period: $T_i=\Period{ck_i}$ ;
  \item Relative deadline: $D_i=T_i$ by default or explicit constraint (eg \lstinline!o:
    due 8!);
  \item Worst case execution time: $C_i$, declared for each imported node;
  \item Initial release date: $O_i=\Phaseof{ck_i}$.
  \end{itemize}


\end{frame}

\begin{frame}
  \frametitle{Multi-rate data-dependencies}
  For each task dependency:
  \begin{enumerate}
  \item \emph{Data can only be consumed after being produced} $\Rightarrow$
    precedence constraints for the scheduler;
  \item \emph{Data must not be overwritten before being consumed}
    $\Rightarrow$ communication protocol.
  \end{enumerate}
  \begin{example}
    \centering
    \input{Figures/respect-dependencies-fig}
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Communication protocol}

  \begin{itemize}
  \item Tailor-made buffering mechanism;
  \item For each dependency, computes:
    \begin{itemize}
    \item Size of the buffer;
    \item Where each job writes/reads;
    \end{itemize}
  \item \emph{Independent of the scheduling policy};
  \item Requires a single central memory.
  \end{itemize}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Communication protocol}
  % \begin{block}{Equations}
  %   \begin{lstlisting}[basicstyle=\scriptsize]
  %     node N(i) returns (o)
  %     var v1,v2;
  %     let
  %     v1=A(i); v2=v1*^3/^2; o=B(v2);      
  %     tel
  %   \end{lstlisting}
  % \end{block}
  Ex: \emph{\lstinline!B(A(x)*^3/^2)!}, ie \emph{$A\precannot{\Uclock 3.\Dclock 2}B$}:

  \begin{block}{Semantics}
    {\scriptsize
      \begin{tabular}{l|cccccccccc}
        \hline
        date & 0 & 10 & 20 & 30 & 40 & 50 & 60 & 70 & 80 & ...\\
        \hline
        \lstinline!A(x)! & $a_0$ & & & $a_1$ & & & $a_2$ &  & & ...\\
        \hline
        \lstinline!A(x)*^3! & $a_0$ & $a_0$ & $a_0$ & $a_1$ & $a_1$ & $a_1$
        & $a_2$ &  $a_2$ & $a_3$ & ...\\
        \hline
        \lstinline!A(x)*^3/^2! & $a_0$ &  & $a_0$ &  & $a_1$ & & $a_2$ & &
        $a_3$ & ...\\
        \hline
      \end{tabular}
    }    
  \end{block}
  
  \begin{block}{Lifespans}
    \begin{center}
      \begin{tikzpicture}[font=\scriptsize,scale=.6]
      \drawClock{12.9}{0}{3}{-0.2}{0.2}{0}{A};
      \drawClock{12.9}{-1}{2}{-1.2}{-0.8}{0}{B};
      \foreach \x in {0,6,12}
      \draw[-latex] (\x,-0.25) -- (\x,-0.75);
      \foreach \x/\xx in {0/2,6/8}
      \draw[-latex] (\x,-0.25) -- (\xx,-0.75);
      \foreach \x/\xx in {3/4,9/10}
      \draw[-latex] (\x,-0.25) -- (\xx,-0.75);
      \foreach \x /\xx /\xtext in {0/4/0,6/10/2}
      \draw[<->] (\x,-1.1) -- node[below] {$\Span_{A,B}(\xtext)$}
      (\xx,-1.1);
      \foreach \x /\xx /\xtext in {3/6/1,9/12/3}
      \draw[<->] (\x,-1.9) -- node[below] {$\Span_{A,B}(\xtext)$} (\xx,-1.9);
    \end{tikzpicture}
    \end{center}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Communication protocol (2)}
  \begin{block}{Lifespans}
    \begin{center}
      \begin{tikzpicture}[font=\scriptsize,scale=.6]
      \drawClock{12.9}{0}{3}{-0.2}{0.2}{0}{A};
      \drawClock{12.9}{-1}{2}{-1.2}{-0.8}{0}{B};
      \foreach \x in {0,6,12}
      \draw[-latex] (\x,-0.25) -- (\x,-0.75);
      \foreach \x/\xx in {0/2,6/8}
      \draw[-latex] (\x,-0.25) -- (\xx,-0.75);
      \foreach \x/\xx in {3/4,9/10}
      \draw[-latex] (\x,-0.25) -- (\xx,-0.75);
      \foreach \x /\xx /\xtext in {0/4/0,6/10/2}
      \draw[<->] (\x,-1.1) -- node[below] {$\Span_{A,B}(\xtext)$}
      (\xx,-1.1);
      \foreach \x /\xx /\xtext in {3/6/1,9/12/3}
      \draw[<->] (\x,-1.9) -- node[below] {$\Span_{A,B}(\xtext)$} (\xx,-1.9);
    \end{tikzpicture}
    \end{center}
  \end{block}

  \begin{itemize}
  \item Buffer of size 2;
  \item Write in the buffer cyclically;
  \item Read from the buffer cyclically;
  \item Do not advance at the same pace for reading and writing.
  \end{itemize}  
  
\end{frame}

\begin{frame}{Scheduling: problem parameters}
  \begin{itemize}
  \item A set of recurring tasks with:
    \begin{itemize}
    \item Periods, deadlines, wcets, release dates;
    \item Multi-rate precedence constraints.
    \end{itemize}
  \item Hardware architecture:
    \begin{itemize}
    \item Mono-core;
    \item Multi-core (with a single central shared memory).
    \end{itemize}
  \item Scheduler class:
    \begin{itemize}
    \item \emph{On-line}/off-line;
    \item Static/dynamic priorities;
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}{Summary}
  What you should remember:
  \begin{itemize}
  \item When we deal with multi-periodic systems, we need explicit
    real-time constraints;
  \item Explicit RT constraints enable:
    \begin{itemize}
    \item Static real-time analyses;
    \item Optimized processor utilization and platform dimensioning.
    \end{itemize}
  \item Real-time constraints can be introduced without breaking the
    synchronous paradigm;
  \item Mixing real time and logical time can be done by using real-time
    as a ``dimension'' for logical time.
  \end{itemize}
\end{frame}

\begin{frame}{My sources}
  Some inspirations for this course:
  \begin{itemize}
  \item \textbf{Fr\'ed\'eric Boniol (ONERA Toulouse)}, Mod\'elisation et
      programmation des syst\`emes embarqu\'es critiques : la voie
      synchrone, \textit{course at Ecole Polytechnique de Montreal, 2013}
    \item \textbf{Emmanuel GROLLEAU (LIAS/ISAE-ENSMA)}, Ordonnancement
      et ordonnan\c{c}abilit\'e monoprocesseur, \textit{Ecole d'Et\'e Temps
      R\'eel (ETR'2011), Brest, 2011}
  \end{itemize}
\end{frame}

\begin{frame}{References}
  \bibliographystyle{plain}
  \nocite{forget10SAC,pagetti11,forget09b,PreludeSite}

  Prelude is a joint work with Fr\'ed\'eric Boniol, David Lesens and Claire Pagetti.

  \bibliography{biblio}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
