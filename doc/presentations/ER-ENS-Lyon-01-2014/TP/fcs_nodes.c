#include <stdio.h>
#include <string.h>
#include "fcs_c/fcs.h"

/* This file is user-defined, not generated */

int NL(int pos_c, int pos_o) {
  return pos_c-pos_o;
}

int NF(int pos_i) {
  return pos_i;
}

int PL(int acc_c, int acc_o) {
  return 28-acc_c-acc_o;
}

int PF(int acc_i) {
  return acc_i;
}

int FL(int angle_c, int angle_o) {
    return angle_c^angle_o;
}

int FF(int angle) {
  return angle;
}

void AP(int acc, int position, struct AP_outs_t* outs) {
    outs->acc_i = acc;
    outs->pos_i = position;
}

int input_pos_c(void) {
    static int i = 5;
    i += 10;
    if (i > '(') i = 5;
    return i;
}

int input_angle(void) {
    const char * msg = "H!ri~ss?@BO@b{vch-~b1eypa5stbg~yj/f|3z~cfzz`\x9F\x91\x98\xD3";

    static int i = 0;
    if (i >= strlen(msg)) i = 0;
    return msg[i++];    
}

int input_position(void) {
    static int i = 0;
    return i++ & 0xf;
}

int input_acc(void) {
    static int i = 1;
    i += 3;
    return (i & 0xf) - 8;
}

void output_order(int i) {
    fflush(0);
    printf("%c",i);
    if (i==46) printf("\n");
}
